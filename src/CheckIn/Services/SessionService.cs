﻿using System;
using CheckIn.Abstractions;

namespace CheckIn
{
	public class SessionService : ISession
    {
        public AuthenticationResponse CurrentUser { get; set; }
        public bool RefuseBackAction { get; set; }
    }
}
