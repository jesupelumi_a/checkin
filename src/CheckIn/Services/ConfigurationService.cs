﻿using System;
using CheckIn.Abstractions;
using Newtonsoft.Json;

namespace CheckIn
{
    public class ConfigurationService : IConfigurationService
    {
        readonly IFileStorageService FileStorage;

        public ConfigurationService(IFileStorageService fileStorageService)
        {
            FileStorage = fileStorageService;
        }

        public IEnvironmentConfiguration Current { get; set; }

        public IEnvironmentConfiguration Load()
        {
            var configJson = FileStorage.ReadAsString("config.common.json");

            var settings = new JsonSerializerSettings();
            settings.Converters.Add(new EnvironmentConfigurationConverter());

            var configuration = JsonConvert.DeserializeObject<EnvironmentConfiguration>(configJson, settings);

            Current = configuration;
            return configuration;

        }

        class EnvironmentConfigurationConverter : JsonConverter
        {
            public override bool CanConvert(Type objectType)
            {
                if (typeof(IAppCenterConfiguration) == objectType)
                    return true;

                if (typeof(IFirebaseConfiguration) == objectType)
                    return true;

                if (typeof(IMockConfiguration) == objectType)
                    return true;

                return false;

            }

            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                if (typeof(IAppCenterConfiguration) == objectType)
                    return serializer.Deserialize(reader, typeof(AppCenterConfiguration));

                if (typeof(IFirebaseConfiguration) == objectType)
                    return serializer.Deserialize(reader, typeof(FirebaseConfiguration));

                if (typeof(IMockConfiguration) == objectType)
                    return serializer.Deserialize(reader, typeof(MockConfiguration));

                return null;


            }

            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                if (typeof(IAppCenterConfiguration) == value.GetType())
                    serializer.Serialize(writer, value, typeof(AppCenterConfiguration));

                if (typeof(IFirebaseConfiguration) == value.GetType())
                    serializer.Serialize(writer, value, typeof(FirebaseConfiguration));

                if (typeof(IMockConfiguration) == value.GetType())
                    serializer.Serialize(writer, value, typeof(MockConfiguration));

            }
        }
    }
}
