﻿using System;
using Acr.UserDialogs;
using CheckIn.Abstractions;
using FreshMvvm;

namespace CheckIn
{
    public static class ContainerConfig
    {
        public static void Load()
        {
            FreshIOC.Container.Register<IConfigurationService, ConfigurationService>();
            FreshIOC.Container.Register<ISession, SessionService>();
        }
    }
}
