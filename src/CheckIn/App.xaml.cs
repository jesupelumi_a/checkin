using System;
using System.Reflection;
using CheckIn.Abstractions;
using FreshMvvm;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using CheckIn.Data.DB;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace CheckIn
{
    public partial class App : Application, IAppService
    {
        IEnvironmentConfiguration Config;

        public App()
        {
            var configurationService = FreshIOC.Container.Resolve<IConfigurationService>();
            Config = configurationService.Current;

            RegisterServices();
            InitializeComponent();
            StartApp();
        }

        protected override void OnStart()
        {
            // Handle when your app starts

            AppCenter.LogLevel = LogLevel.Verbose;
            AppCenter.Start("android=0a185b79-2ec8-4a84-ba92-7136149b0f6a;ios=",
                        typeof(Analytics), typeof(Crashes));
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        public void StartApp()
        {
            Page page = new Page();
            FreshNavigationContainer container;

            //page = FreshPageModelResolver.ResolvePageModel<UnitTestsViewModel>();
            page = FreshPageModelResolver.ResolvePageModel<LoginViewModel>();

            container = new FreshNavigationContainer(page);
            MainPage = container;
        }

        public void StartMainFlow()
        {
            var page = FreshPageModelResolver.ResolvePageModel<AttendanceViewModel>();
            var container = new FreshNavigationContainer(page, Guid.NewGuid().ToString());
            this.MainPage = container;
        }

        public void LogOut()
        {
            var session = FreshIOC.Container.Resolve<ISession>();
            session.CurrentUser = null;
            session.RefuseBackAction = false;
            StartApp();
        }

        private void RegisterServices()
        {
            FreshIOC.Container.Register<IAppService>(this);
            FreshIOC.Container.Register<IErrorManager, ErrorManager>();
            FreshIOC.Container.Register<Acr.UserDialogs.IUserDialogs>(Acr.UserDialogs.UserDialogs.Instance);
            //FreshIOC.Container.Register<ISession, SessionService>();

            if (Config.Mock.Enabled)
            {
                FreshIOC.Container.Register<IAuthenticationService, Data.Mock.AuthenticationService>();
                FreshIOC.Container.Register<IUserService, Data.Mock.UserService>();
            }
            else
            {
                FreshIOC.Container.Register<IAuthenticationService, Data.DB.FirebaseAuthService>();
                FreshIOC.Container.Register<IUserService, Data.DB.FirebaseUserService>();
            }

            //registering svg images resource folder
            var assembly = typeof(App).GetTypeInfo().Assembly;
            XamSvg.Shared.Config.ResourceAssembly = assembly;
        }

        public static bool refuseBack()
        {
            var session = FreshIOC.Container.Resolve<ISession>();
            return session.RefuseBackAction;
        }
    }
}
