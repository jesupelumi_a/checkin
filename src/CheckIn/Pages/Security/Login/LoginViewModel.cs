﻿using System;
using CheckIn.Abstractions;
using PropertyChanged;
using System.Threading.Tasks;
using Xamarin.Forms;
using Acr.UserDialogs;
using Plugin.Connectivity;

namespace CheckIn
{
    [AddINotifyPropertyChangedInterface]
    public class LoginViewModel : BaseViewModel
    {
        readonly IErrorManager ErrorManager;
        readonly IAuthenticationService AuthenticationService;
        readonly ISession SessionService;
        readonly IUserService EmployeeService;
        readonly IAppService AppService;
        readonly IUserDialogs Dialog;
        readonly IUserService UserService;

        public string UserName { get; set; }
        public string Password { get; set; }
        public string ErrorMessage { get; set; }

        public Command Login { get; set; }

        public LoginViewModel(IErrorManager errorManager, IAuthenticationService authenticationService, ISession sessionService,
                              IUserService employeeService, IAppService appService, IUserDialogs dialog, IUserService userService)
        {
            ErrorManager = errorManager;
            AuthenticationService = authenticationService;
            SessionService = sessionService;
            EmployeeService = employeeService;
            AppService = appService;
            Dialog = dialog;
            UserService = userService;

            Login = new Command(async () => await ExecuteLogin());
        }

        private async Task ExecuteLogin()
        {
            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    await CoreMethods.DisplayAlert("Whoops!", "Seems you don't have internet access.", "Ok");
                    return;
                }

                if (!ValidateCredentials())
                    await CoreMethods.DisplayAlert("Validation Error", ErrorMessage, "Ok");
                else
                    Authenticate(UserName, Password);

            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }

        private async void Authenticate(string username, string password)
        {
            try
            {
                using (UserDialogs.Instance.Loading("Authenticating...", null, null, true, MaskType.Gradient))
                {
                    var response = await AuthenticationService.Authenticate(username, password);

                    if (response.IsAuthenticated)
                    {
                        SessionService.CurrentUser = response;

                        AppService.StartMainFlow();

                        SessionService.RefuseBackAction = true;
                    }
                    else
                    {
                        await CoreMethods.DisplayAlert("Access Denied!", "Please check your username and password", "Ok");
                    }
                }
            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }

        private bool ValidateCredentials()
        {
            bool retVal = true;

            if (string.IsNullOrEmpty(UserName))
            {
                ErrorMessage = "Please enter your username to continue";
                retVal = false;
            }
            else if (string.IsNullOrEmpty(Password))
            {
                ErrorMessage = "Please enter your password to continue";
                retVal = false;
            }

            return retVal;
        }
    }
}
