﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CheckIn
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }


        async void Handle_Focused(object sender, Xamarin.Forms.FocusEventArgs e)
        {
            var entry = sender as Entry;

            if (entry != null)
            {
                switch (entry.Placeholder.ToLower())
                {
                    case "username":
                        await scroll.ScrollToAsync(password, ScrollToPosition.Center, true);
                        break;                    
                }
            }
        }

        private void password_Focused(object sender, FocusEventArgs e)
        {

        }
    }
}
