﻿using System;
using System.Threading.Tasks;
using CheckIn.Abstractions;
using Xamarin.Forms;

namespace CheckIn
{
    public class AttendanceItemViewModel
    {
        readonly IErrorManager ErrorManager;
        readonly IUserService UserService;
        AttendanceViewModel Parent;
        public DateTime? Date { get; set; }
        public string Time { get; set; }
        public string Status { get; set; }
        public string UserID { get; set; }

        public Command AddAttendance { get; set; }

        public AttendanceItemViewModel(IErrorManager errorManager, IUserService userService, AttendanceViewModel parent, AttendanceModel model)
        {
            ErrorManager = errorManager;
            UserService = userService;
            Parent = parent;
            Date = model.Date;
            Time = model.Time;
            Status = model.Status;
            UserID = parent.UserID;

            AddAttendance = new Command<Attendance>(async (obj) => await ExecuteAddAttendance(obj));
        }

        private async Task ExecuteAddAttendance(Attendance obj)
        {
            try
            {
                //Add attendance to firebase
                await UserService.AddAttendance(obj);
            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }
    }
}
