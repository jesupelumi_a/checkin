﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CheckIn
{
    public partial class AttendancePage : ContentPage
    {
        public AttendancePage()
        {
            InitializeComponent();
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = sender as ListView;
            listView.SelectedItem = null;
        }

        void Handle_DatePicker(object sender, System.EventArgs e)
        {
            datepicker.Focus();
        }

        void Handle_DateSelected(object sender, Xamarin.Forms.DateChangedEventArgs e)
        {
            var model = BindingContext as AttendanceViewModel;
            model.SelectedDate = datepicker.Date;
            model.SelectDate.Execute(null);
        }
    }
}
