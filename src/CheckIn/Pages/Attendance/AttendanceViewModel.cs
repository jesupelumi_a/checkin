﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Acr.UserDialogs;
using CheckIn.Abstractions;
using Plugin.Connectivity;
using PropertyChanged;
using Xamarin.Forms;

namespace CheckIn
{
    [AddINotifyPropertyChangedInterface]
    public class AttendanceViewModel : BaseViewModel
    {
        readonly IErrorManager ErrorManager;
        readonly IUserService UserService;
        readonly ISession SessionService;
        readonly IAppService AppService;
        readonly IUserDialogs Dialog;

        public ObservableCollection<AttendanceItemViewModel> AttendanceSource { get; set; }
        public List<Attendance> Attendances { get; set; }

        public DateTime SelectedDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime Today { get { return DateTime.Now; } }
        public string UserID { get; set; }
        public string DateRange { get; set; }
        public string WelcomeMessage { get; set; }
        public bool IsRecordNotAvailable { get; set; }

        public Command SelectDate { get; set; }
        public Command Logout { get; set; }

        public AttendanceViewModel(IErrorManager errorManager, IUserService userService, ISession sessionService, 
                                    IAppService appService, IUserDialogs dialog)
        {
            ErrorManager = errorManager;
            UserService = userService;
            SessionService = sessionService;
            AppService = appService;
            Dialog = dialog;

            SelectDate = new Command(async () => await ExecuteSelectDate());
            Logout = new Command(async () => await ExecuteLogout());
        }

        public async override void Init(object initData)
        {
            base.Init(initData);

            try
            {
                using (UserDialogs.Instance.Loading("Fetching data...", null, null, true, MaskType.Gradient))
                {
                    SelectedDate = DateTime.Now;
                    UserID = SessionService?.CurrentUser?.Id;
                    AttendanceSource = await GetData();
                    WelcomeMessage = $"Welcome, {SessionService?.CurrentUser?.FirstName}";
                    IsRecordNotAvailable = AttendanceSource.Count == 0;
                    StartDate = StartDate.Date == DateTime.Now.Date ? DateTime.Now.AddDays(-1) : StartDate;
                }
            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }

        public async Task<ObservableCollection<AttendanceItemViewModel>> GetData()
        {
            try
            {
                var list = new List<AttendanceItemViewModel>();

                //Get Attendances from db
                Attendances = await UserService.GetAttendancesByUserID(UserID);
                                
                StartDate = Attendances.Count > 0 ? Attendances.OrderBy(x => x.Date).First().Date : SelectedDate ;
                EndDate = Attendances.Count > 0 ? Attendances.OrderBy(x => x.Date).Last().Date : SelectedDate;

                //List out all dates from start to end
                var dates = GetDaysFromSelectedDate(SelectedDate);

                //Remove all dates greater than today and less than start date
                dates = dates.Where(x => x.Date >= StartDate.Date && x.Date <= DateTime.Now.Date);

                //Remove weekends
                dates = RemoveWeekendDates(dates.ToList());

                DateRange = dates.Count() > 1 ? $"{dates.First().ToString("MMM. dd, yyyy")} - {dates.Last().ToString("MMM. dd, yyyy")}" 
                            : dates.Count() == 1 ? $"{dates.First().ToString("MMM. dd, yyyy")}" 
                                                    : $"{DateTime.Now.ToString("MMM. dd, yyyy")}";
                //Fill list
                foreach (var date in dates)
                {
                    var attendance = Attendances.FirstOrDefault(x => x.Date.Date == date.Date);
                    var model = new AttendanceModel();
                    model.Date = date;
                    model.Status = attendance != null ? AttendanceStatus.Present : attendance == null && date.Date == DateTime.Now.Date ?
                                                        AttendanceStatus.Pending : AttendanceStatus.Absent;
                    model.Time = attendance != null ? attendance.Time : "";

                    list.Add(new AttendanceItemViewModel(ErrorManager, UserService, this, model));
                }

                var data = new ObservableCollection<AttendanceItemViewModel>(list);
                return await Task.FromResult(data);
            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
                return null;
            }
        }

        private async Task ExecuteSelectDate()
        {
            try
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    await CoreMethods.DisplayAlert("Whoops!", "Seems you don't have internet access.", "Ok");
                    return;
                }

                using (UserDialogs.Instance.Loading("Fetching data...", null, null, true, MaskType.Gradient))
                {
                    AttendanceSource = await GetData();
                }
            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }

        private async Task ExecuteLogout()
        {
            try
            {
                var message = "Are you sure you want to logout?";
                var title = "Logout";

                var isOkSelected = await Dialog.ConfirmAsync(message, title);

                if (isOkSelected)
                {
                    AttendanceSource = null;
                    AppService.LogOut();
                }
            }
            catch (Exception ex)
            {
                await ErrorManager.DisplayErrorMessageAsync(ex);
            }
        }

        private List<DateTime> RemoveWeekendDates(List<DateTime> obj)
        {
            var retVal = obj.Where(x => x.Date.DayOfWeek != DayOfWeek.Sunday && x.Date.DayOfWeek != DayOfWeek.Saturday);
            return retVal.ToList();
        }

        public static IEnumerable<DateTime> GetDaysFromSelectedDate(DateTime? checkDay = null)
        {
            var compDate = checkDay ?? DateTime.UtcNow;
            var days = DayOfWeek.Monday - compDate.DayOfWeek;
            days = days > 0 ? days - 7 : days;
            var startDate = compDate.AddDays(days);

            for (var i = 0; i < 7; i++)
            {
                yield return startDate.AddDays(i).Date;
            }
        }
    }
}
