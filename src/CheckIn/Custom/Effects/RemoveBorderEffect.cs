﻿using System;
using Xamarin.Forms;

namespace CheckIn
{
    public class RemoveBorderEffect : RoutingEffect
    {
        public RemoveBorderEffect() : base("CheckIn.RemoveBorderEffect")
        {
        }
    }
}
