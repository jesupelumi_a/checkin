﻿using System;
using Xamarin.Forms;

namespace CheckIn
{
    public class UnderlineEffect : RoutingEffect
    {
        public UnderlineEffect() : base("CheckIn.UnderlineEffect")
        {
        }
    }
}
