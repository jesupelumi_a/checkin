﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Acr.UserDialogs;
using CheckIn.Abstractions;
using Plugin.Connectivity;
using Xamarin.Forms;

namespace CheckIn
{
    public partial class RecordCell : ContentView
    {
        readonly IErrorManager ErrorManager;
        readonly IUserDialogs Dialog;

        TimePicker TimePicker;

        public RecordCell()
        {
            InitializeComponent();

            ErrorManager = FreshMvvm.FreshIOC.Container.Resolve<IErrorManager>();
            Dialog = FreshMvvm.FreshIOC.Container.Resolve<IUserDialogs>();

            var tap = new TapGestureRecognizer();
            tap.Tapped += Handle_Tapped;
            this.GestureRecognizers.Add(tap);
        }

        #region Date

        public static readonly BindableProperty DateProperty = BindableProperty.Create("Date", typeof(DateTime?), typeof(RecordCell), null);

        public DateTime? Date
        {
            get { return (DateTime?)GetValue(DateProperty); }
            set { SetValue(DateProperty, value); }
        }

        private void SetDate()
        {
            try
            {
                if (Date != null)
                {
                    var date = Date.GetValueOrDefault();
                    monthValue.Text = date.Day.ToString();
                    dayOfWeek.Text = date.DayOfWeek.ToString();
                    monthText.Text = date.ToString("MMM yyyy");
                }
            }
            catch { }
        }

        #endregion

        #region Time

        public static readonly BindableProperty TimeProperty = BindableProperty.Create("Time", typeof(string), typeof(RecordCell), default(string), BindingMode.TwoWay);

        public string Time
        {
            get { return (string)GetValue(TimeProperty); }
            set { SetValue(TimeProperty, value); }
        }

        private void SetTime()
        {
            try
            {
                if (!string.IsNullOrEmpty(Time))
                    time.Text = Time.ToUpper();
                else
                    time.Text = "--:--";
            }
            catch { }
        }

        #endregion

        #region Status

        public static readonly BindableProperty StatusProperty = BindableProperty.Create("Status", typeof(string), typeof(RecordCell), default(string), BindingMode.TwoWay);

        public string Status
        {
            get { return (string)GetValue(StatusProperty); }
            set { SetValue(StatusProperty, value); }
        }

        private void SetStatus()
        {
            try
            {
                switch(Status)
                {
                    case "Present":
                        flag.BackgroundColor = Color.FromHex("#21A977");
                        break;
                    case "Absent":
                        flag.BackgroundColor = Color.FromHex("#CF4941");
                        break;
                    case "Pending":
                        flag.BackgroundColor = Color.FromHex("#FFD35C");
                        break;
                }
            }
            catch { }
        }

        #endregion

        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == DateProperty.PropertyName)
            {
                SetDate();
            }

            if (propertyName == TimeProperty.PropertyName)
            {
                SetTime();
            }

            if (propertyName == StatusProperty.PropertyName)
            {
                SetStatus();
            }
        }

        async void Handle_Tapped(object sender, System.EventArgs e)
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                await Dialog.AlertAsync("Seems you don't have internet access.", "Whoops!", "Ok");
                return;
            }

            if (Status != Abstractions.AttendanceStatus.Present)
            {
                TimePicker = new TimePicker();
                TimePicker.Unfocused += Picker_Unfocused;
                TimePicker.IsVisible = false;
                grid.Children.Add(TimePicker);
                TimePicker.Focused += Picker_Focused;
                TimePicker.Unfocused += Picker_Unfocused;
                TimePicker.Focus();
            }
        }

        private void Picker_Focused(object sender, FocusEventArgs e)
        {
            try
            {
                TimePicker.PropertyChanged += PickerPropertyChanged;
                //TimePicker = null;
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"===============Picker_Focused: {ex.Message}===========");
            }
        }

        private void Picker_Unfocused(object sender, FocusEventArgs e)
        {
            try
            {
                TimePicker.Unfocus();
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"===============Picker_Unfocused: {ex.Message}===========");
            }
        }

        void PickerPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            try
            {
                if (Status == Abstractions.AttendanceStatus.Present)
                    return;

                if (TimePicker.Time != new TimeSpan(12, 00, 12))
                {
                    using (Dialog.Loading("Saving..."))
                    {
                        Status = AttendanceStatus.Pending;
                        DateTime _time = DateTime.Today.Add(TimePicker.Time);
                        Time = _time.ToString("hh:mm tt");

                        var model = this.BindingContext as AttendanceItemViewModel;
                        var attendance = new Attendance { Date = Date.GetValueOrDefault(), Time = Time, UserID = model.UserID };
                        model.AddAttendance.Execute(attendance);
                        Status = AttendanceStatus.Present;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"===============OnTimePickerPropertyChanged: {ex.Message}===========");
            }
        }
    }
}
