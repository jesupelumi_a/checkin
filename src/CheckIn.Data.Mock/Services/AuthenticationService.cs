﻿using System;
using System.Linq;
using System.Threading.Tasks;
using CheckIn.Abstractions;

namespace CheckIn.Data.Mock
{
    public class AuthenticationService : IAuthenticationService
    {
        readonly IUserService UserService;

        public AuthenticationService(IUserService userService)
        {
            UserService = userService;
        }

        public async Task<AuthenticationResponse> Authenticate(string username, string password)
        {
            AuthenticationResponse response = await ReturnValidResponse(username, password);
            return await Task.FromResult(response);
        }

        public async Task<AuthenticationResponse> ReturnValidResponse(string username, string password)
        {
            var response = new AuthenticationResponse();

            var employees = await UserService.GetUsers();
            var employee = employees.FirstOrDefault(x => x.UserName.ToLower() == username.ToLower());

            if (employee == null)
            {
                response.IsAuthenticated = false;
            }
            else
            {
                if (password == employee.Password)
                {
                    response.UserName = employee.UserName;
                    response.FirstName = employee.FirstName;
                    response.LastName = employee.LastName;
                    response.IsAuthenticated = true;
                }
                else
                {
                    response.IsAuthenticated = false;
                }
            }

            return response;
        }
    }
}
