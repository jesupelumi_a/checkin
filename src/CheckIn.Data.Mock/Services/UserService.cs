﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CheckIn.Abstractions;

namespace CheckIn.Data.Mock
{
    public class UserService : IUserService
    {
        ISession Session;

        List<User> users = new List<User>();
        List<Attendance> attendances = new List<Attendance>();

        public UserService(ISession session)
        {
            Session = session;

            SetupAttendances();
            SetupUsers();
        }

        public async Task<List<User>> GetUsers()
        {
            return await Task.FromResult(users);
        }

        public async Task<List<Attendance>> GetAttendancesByUserID(string userId)
        {
            var retVal = attendances.Where(x => x.UserID == userId).ToList();
            return await Task.FromResult(retVal);
        }

        public async Task<bool> AddAttendance(Attendance attendance)
        {
            attendances.Add(attendance);
            await Task.Delay(20000);
            return await Task.FromResult(true);
        }

        private void SetupUsers()
        {
            users.AddRange(new List<User>
            {
                new User { Id = "1", FirstName = "John", LastName = "Doe", UserName = "JohnD", Password = "password" },
                new User { Id = "2", FirstName = "Jane", LastName = "Doe", UserName = "JaneD", Password = "password" }
            });
        }

        private void SetupAttendances()
        {
            attendances.AddRange(new List<Attendance>
            {
                new Attendance { Date = DateTime.Now.AddDays(-1), Time = "08:00 AM", UserID = "1" },
                new Attendance { Date = DateTime.Now.AddDays(-3), Time = "09:00 AM", UserID = "1" },
                new Attendance { Date = DateTime.Now.AddDays(-4), Time = "09:00 AM", UserID = "1" },
                new Attendance { Date = DateTime.Now.AddDays(-5), Time = "09:00 AM", UserID = "1" },
                                                                                    
                new Attendance { Date = DateTime.Now.AddDays(-7), Time = "08:00 AM", UserID = "1"},
                new Attendance { Date = DateTime.Now.AddDays(-9), Time = "09:00 AM", UserID = "1"},
                new Attendance { Date = DateTime.Now.AddDays(-10), Time = "09:00 AM", UserID = "1" },
                new Attendance { Date = DateTime.Now.AddDays(-11), Time = "09:00 AM", UserID = "1" },

                new Attendance { Date = DateTime.Now.AddDays(-15), Time = "08:00 AM", UserID = "1" },
                new Attendance { Date = DateTime.Now.AddDays(-13), Time = "09:00 AM", UserID = "1" },
                new Attendance { Date = DateTime.Now.AddDays(-12), Time = "09:00 AM", UserID = "1" },
                new Attendance { Date = DateTime.Now.AddDays(-16), Time = "09:00 AM", UserID = "1" },
            });
        }
    }
}
