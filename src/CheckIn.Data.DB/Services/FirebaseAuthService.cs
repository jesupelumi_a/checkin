﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CheckIn.Abstractions;
using Firebase.Database;

namespace CheckIn.Data.DB
{
    public class FirebaseAuthService : IAuthenticationService
    {
        readonly FirebaseUserService FirebaseUserService;
        List<User> users = new List<User>();

        public FirebaseAuthService(FirebaseUserService firebaseUserService)
        {
            FirebaseUserService = firebaseUserService;
        }

        public async Task<AuthenticationResponse> Authenticate(string username, string password)
        {
            try
            {
                var response = new AuthenticationResponse();

                users = await FirebaseUserService.GetUsers();

                response = GetResponse(username, password);

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private AuthenticationResponse GetResponse(string username, string password)
        {
            var response = new AuthenticationResponse();
            var user = users.FirstOrDefault(x => x.UserName.ToLower() == username.ToLower() && x.Password == password);

            if (user != null)
            {
                response.Id = user.Id;
                response.FirstName = user.FirstName;
                response.LastName = user.LastName;
                response.UserName = user.UserName;
                response.IsAuthenticated = true;
            }
            else
            {
                response.IsAuthenticated = false;
            }

            return response;
        }
    }
}
