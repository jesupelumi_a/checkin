﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CheckIn.Abstractions;
using Firebase.Database;
using Newtonsoft.Json;

namespace CheckIn.Data.DB
{
    public class FirebaseUserService : IUserService
    {
        private readonly IEnvironmentConfiguration Config;
        FirebaseClient firebase;
        List<Attendance> attendances = new List<Attendance>();
        List<User> users = new List<User>();

        public FirebaseUserService(IConfigurationService configurationService)
        {
            Config = configurationService.Current;

            firebase = new FirebaseClient(Config.Firebase.BaseUrl);
        }

        public async Task<bool> AddAttendance(Attendance attendance)
        {
            var _attendance = JsonConvert.SerializeObject(attendance);

            var item = await firebase.Child("attendances").PostAsync(_attendance);

            return !string.IsNullOrEmpty(item.Key);
        }

        public async Task<List<Attendance>> GetAttendancesByUserID(string Id)
        {
            var dbList = await firebase.Child("attendances").OnceAsync<Attendance>();
            attendances = dbList.Select(item => new Attendance
            {
                Date = item.Object.Date,
                Time = item.Object.Time,
                UserID = item.Object.UserID
            }).ToList();
            attendances = attendances.Where(x => x.UserID == Id).ToList();
            return attendances;
        }

        public async Task<List<User>> GetUsers()
        {
            var dbList = await firebase.Child("users").OnceAsync<User>();
            users = dbList.Select(item => new User
            {
                Id = item.Object.Id,
                FirstName = item.Object.FirstName,
                LastName = item.Object.LastName,
                UserName = item.Object.UserName,
                Password = item.Object.Password
            }).ToList();
            return users;
        }
    }
}
