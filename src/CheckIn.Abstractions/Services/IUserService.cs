﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CheckIn.Abstractions
{
    public interface IUserService
    {
        Task<List<Attendance>> GetAttendancesByUserID(string username);
        Task<bool> AddAttendance(Attendance attendance);
        Task<List<User>> GetUsers();
    }
}
