﻿using System;

namespace CheckIn.Abstractions
{
    public interface IFileStorageService
    {
        string ReadAsString(string fileName);
    }
}
