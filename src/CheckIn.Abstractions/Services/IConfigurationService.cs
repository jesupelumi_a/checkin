﻿using System;
namespace CheckIn.Abstractions
{
    public interface IConfigurationService
    {
        IEnvironmentConfiguration Current { get; }
        IEnvironmentConfiguration Load();
    }
}
