﻿using System;
using System.Threading.Tasks;

namespace CheckIn.Abstractions
{
    public interface IAuthenticationService
    {
        Task<AuthenticationResponse> Authenticate(string username, string password);
    }
}
