﻿using System;
namespace CheckIn.Abstractions
{
    public interface ISession
    {
        AuthenticationResponse CurrentUser { get; set; }
        bool RefuseBackAction { get; set; }
    }
}
