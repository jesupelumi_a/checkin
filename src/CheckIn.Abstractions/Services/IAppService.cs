﻿using System;
namespace CheckIn.Abstractions
{
    public interface IAppService
    {
        void StartMainFlow();
        void LogOut();
    }
}
