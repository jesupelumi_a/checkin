﻿using System;
namespace CheckIn.Abstractions
{
    public class AttendanceModel
    {
        public DateTime? Date { get; set; }
        public string Time { get; set; }
        public string Status { get; set; }
        public int WeekOfYear { get; set; }
    }
}
