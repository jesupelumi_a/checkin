﻿using System;
using Newtonsoft.Json;

namespace CheckIn.Abstractions
{
    public class Attendance
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("time")]
        public string Time { get; set; }        

        [JsonProperty("userId")]
        public string UserID { get; set; }
    }
}
