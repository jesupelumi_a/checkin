﻿using System;
namespace CheckIn.Abstractions
{
    public class AuthenticationResponse
    {
        public bool IsAuthenticated { get; set; }

        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
    }
}
