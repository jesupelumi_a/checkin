﻿using System;
namespace CheckIn.Abstractions
{
    public class AttendanceStatus
    {
        public const string Present = "Present";
        public const string Absent = "Absent";
        public const string Pending = "Pending";
    }
}
