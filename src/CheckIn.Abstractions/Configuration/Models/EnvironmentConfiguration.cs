﻿using System;
namespace CheckIn.Abstractions
{
    public class EnvironmentConfiguration : IEnvironmentConfiguration
    {
        public string Api { get; set; }
        public IAppCenterConfiguration AppCenter { get; set; }
        public IFirebaseConfiguration Firebase { get; set; }
        public IMockConfiguration Mock { get; set; }
    }
}
