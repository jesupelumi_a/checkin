﻿using System;
namespace CheckIn.Abstractions
{
    public interface IEnvironmentConfiguration
    {
        string Api { get; }
        IAppCenterConfiguration AppCenter { get; }
        IFirebaseConfiguration Firebase { get; }
        IMockConfiguration Mock { get; }
    }
}
