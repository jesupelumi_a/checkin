﻿using System;
namespace CheckIn.Abstractions
{
    public interface IAppCenterConfiguration
    {
        string iOS { get; }
        string Droid { get; }
    }
}
