﻿using System;
namespace CheckIn.Abstractions
{
    public class FirebaseConfiguration : IFirebaseConfiguration
    {
        public string BaseUrl { get; set; }
    }
}
