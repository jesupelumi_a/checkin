﻿using System;
namespace CheckIn.Abstractions
{
    public class MockConfiguration : IMockConfiguration
    {
        public bool Enabled { get; set; }
        public bool DisplayUnitTests { get; set; }
    }
}
