﻿using System;
namespace CheckIn.Abstractions
{
    public interface IMockConfiguration
    {
        bool Enabled { get; }
        bool DisplayUnitTests { get; }
    }
}
