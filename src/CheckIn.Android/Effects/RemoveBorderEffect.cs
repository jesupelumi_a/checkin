﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using CheckIn.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ResolutionGroupName("CheckIn")]
[assembly: ExportEffect(typeof(RemoveBorderEffect), "RemoveBorderEffect")]
namespace CheckIn.Droid
{
    public class RemoveBorderEffect : PlatformEffect
    {
        protected override void OnAttached()
        {
            try
            {
                if (Control != null)
                {
                    Control.SetBackground(null);
                    Control.SetPadding(0, 0, 0, 0);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error during OnAttach of NoBorderStyleEffect - {0}", ex.Message);
            }
        }

        protected override void OnDetached()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Console.WriteLine("Error during OnDetach of NoBorderStyleEffect - {0}", ex.Message);
            }
        }
    }
}