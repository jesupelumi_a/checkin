﻿using System;
using CheckIn.Abstractions;
using FreshMvvm;

namespace CheckIn.Droid
{
    public static class ContainerConfig
    {
        public static void Load()
        {
            FreshIOC.Container.Register<IFileStorageService, AndroidFileStorageService>();

            CheckIn.ContainerConfig.Load();
        }
    }
}
